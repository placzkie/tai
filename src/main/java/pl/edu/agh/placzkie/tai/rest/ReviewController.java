package pl.edu.agh.placzkie.tai.rest;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import pl.edu.agh.placzkie.tai.domain.google.GooglePlaceDetailsResult;
import pl.edu.agh.placzkie.tai.domain.google.SearchResult;
import pl.edu.agh.placzkie.tai.places.PlacesRequester;

/**
 * Created by Leszek Placzkiewicz on 2016-04-30.
 */

@RestController
@RequestMapping("/review")
public class ReviewController {
    private PlacesRequester placesRequester;

    public ReviewController() {
        this.placesRequester = new PlacesRequester();
    }

    @RequestMapping(method = RequestMethod.GET)
    public GooglePlaceDetailsResult getGoogleReviews(@RequestParam(value = "placeID") String placeID) {
        return placesRequester.requestPlaceDetails(placeID).getGooglePlaceDetailsResult();
    }

    @RequestMapping(path = "/search", method = RequestMethod.GET)
    public SearchResult findPlaceId(@RequestParam(value = "lat") double lat, @RequestParam(value = "lng") double lng) {
        return placesRequester.requestPlaceId(lat, lng);
    }
}