package pl.edu.agh.placzkie.tai.domain.notes;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * Created by Leszek Placzkiewicz on 2016-05-07.
 */
@Entity
public class Note {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private long id;
    private String placeID;
    private String userName;
    private String text;

    public Note() {}

    public Note(String text, String placeID, String userName) {
        this.text = text;
        this.placeID = placeID;
        this.userName = userName;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getPlaceID() {
        return placeID;
    }

    public void setPlaceID(String placeID) {
        this.placeID = placeID;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    @Override
    public String toString() {
        return "user=" + userName + ", placeID=" + placeID + ", text=" + text;
    }
}
