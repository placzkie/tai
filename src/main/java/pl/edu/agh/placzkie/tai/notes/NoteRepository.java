package pl.edu.agh.placzkie.tai.notes;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import pl.edu.agh.placzkie.tai.domain.notes.Note;

import java.util.List;

/**
 * Created by Leszek Placzkiewicz on 2016-05-07.
 */
public interface NoteRepository extends CrudRepository<Note, Long> {

    @Query("select distinct n.placeID from Note n where n.userName = ?1")
    List<String> findPlacesByUser(String userName);

    @Query("select n from Note n where n.userName = ?1 and n.placeID = ?2")
    List<Note> getUserNotesForPlace(String userName, String placeID);
}
