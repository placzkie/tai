package pl.edu.agh.placzkie.tai.domain.google;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Leszek Placzkiewicz on 2016-04-30.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Review {

    @JsonProperty("author_name")
    private String authorName;
    private String text;

    public String getAuthorName() {
        return authorName;
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return authorName + " : " + text;
    }
}
