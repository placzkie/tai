package pl.edu.agh.placzkie.tai.domain.google;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * Created by Leszek Placzkiewicz on 2016-05-01.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class GooglePlaceDetailsResult {
    private String name;
    private float rating;
    @JsonProperty("formatted_address")
    private String formattedAddress;
    private List<Review> reviews;
    private Object geometry;

    public Object getGeometry() {
        return geometry;
    }

    public void setGeometry(Object geometry) {
        this.geometry = geometry;
    }

    public String getFormattedAddress() {
        return formattedAddress;
    }

    public void setFormattedAddress(String formattedAddress) {
        this.formattedAddress = formattedAddress;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }

    public List<Review> getReviews() {
        return reviews;
    }

    public void setReviews(List<Review> reviews) {
        this.reviews = reviews;
    }

    @Override
    public String toString() {
        return "name = " + name +
                ", rating = " + rating +
                ", reviews = " + reviews;
    }
}
