package pl.edu.agh.placzkie.tai.domain.google;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by Leszek Placzkiewicz on 2016-05-22.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class SearchResult {
    private Object[] results;

    public Object[] getResults() {
        return results;
    }

    public void setResults(Object[] results) {
        this.results = results;
    }
}
