package pl.edu.agh.placzkie.tai.places;

import org.springframework.web.client.RestTemplate;
import pl.edu.agh.placzkie.tai.domain.google.PlaceDetails;
import pl.edu.agh.placzkie.tai.domain.google.SearchResult;

/**
 * Created by Leszek Placzkiewicz on 2016-04-30.
 */
public class PlacesRequester {
    private final String GOOGLE_PLACES_PATH = "https://maps.googleapis.com/maps/api/place";
    private final String PLACE_DETAILS_PATH = GOOGLE_PLACES_PATH + "/details/json";
    private final String NEARBY_SEARCH_PATH = GOOGLE_PLACES_PATH + "/nearbysearch/json";
    private final String PLACE_ID = "placeid";
    private final String KEY = "key";
    private final RestTemplate restTemplate;
    private final String googlePlacesKey;

    public PlacesRequester() {
        this.restTemplate = new RestTemplate();
        this.googlePlacesKey = System.getProperty("googleKey");
    }

    public PlaceDetails requestPlaceDetails(String placeID) {
        String url = prepareDetailsUrl(placeID);
        System.out.println(url);
        return restTemplate.getForObject(url, PlaceDetails.class);
    }

    public SearchResult requestPlaceId(double longitude, double latitude) {
        String url = prepareNearbySearchUrl(longitude, latitude);
        System.out.println(url);
        return restTemplate.getForObject(url, SearchResult.class);
    }

    private String prepareNearbySearchUrl(double longitude, double latitude) {
        return NEARBY_SEARCH_PATH + "?location=" + longitude + "," + latitude + "&radius=1" + "&" + KEY + "=" + googlePlacesKey;
    }

    private String prepareDetailsUrl(String placeID) {
        return PLACE_DETAILS_PATH + "?" + PLACE_ID + "=" + placeID + "&" + KEY + "=" + googlePlacesKey;
    }
}
