package pl.edu.agh.placzkie.tai.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.edu.agh.placzkie.tai.domain.notes.Note;
import pl.edu.agh.placzkie.tai.notes.NoteRepository;

import java.util.List;

/**
 * Created by Leszek Placzkiewicz on 2016-05-07.
 */
@RestController
@RequestMapping("/note")
public class NotesController {
    @Autowired
    private NoteRepository noteRepository;

    public NotesController() {}

    public NotesController(NoteRepository noteRepository) {
        this.noteRepository = noteRepository;
    }

    @RequestMapping(path = "{userName}/all", method = RequestMethod.GET)
    public List<String> getUserNotes(@PathVariable("userName") String userName) {
        return noteRepository.findPlacesByUser(userName);
    }

    @RequestMapping(path = "{userName}/{placeID}", method = RequestMethod.GET)
    public List<Note> getNotesForPlace(@PathVariable("userName") String userName,
                                         @PathVariable("placeID") String placeID) {
        return noteRepository.getUserNotesForPlace(userName, placeID);
    }

    @RequestMapping(path = "{userName}/{placeID}", method = RequestMethod.POST)
    public ResponseEntity<?> addNote(@PathVariable("userName") String userName,
                                     @PathVariable("placeID") String placeID,
                                     @RequestBody String text) {
        noteRepository.save(new Note(text, placeID, userName));
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @RequestMapping(path = "{noteID}", method = RequestMethod.DELETE)
    public ResponseEntity<?> removeNote(@PathVariable(value = "noteID") long noteID) {
        if(noteRepository.exists(noteID)) {
            noteRepository.delete(noteID);
        }
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @RequestMapping(path = "{noteID}", method = RequestMethod.PUT)
    public ResponseEntity<?> updateNote(@PathVariable(value = "noteID") long noteID, @RequestBody String text) {
        Note note = noteRepository.findOne(noteID);
        note.setText(text);
        noteRepository.save(note);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
