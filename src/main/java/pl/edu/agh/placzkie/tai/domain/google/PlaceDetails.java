package pl.edu.agh.placzkie.tai.domain.google;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Leszek Placzkiewicz on 2016-04-30.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class PlaceDetails {
    @JsonProperty("result")
    private GooglePlaceDetailsResult googlePlaceDetailsResult;

    public GooglePlaceDetailsResult getGooglePlaceDetailsResult() {
        return googlePlaceDetailsResult;
    }

    public void setGooglePlaceDetailsResult(GooglePlaceDetailsResult googlePlaceDetailsResult) {
        this.googlePlaceDetailsResult = googlePlaceDetailsResult;
    }

    @Override
    public String toString() {
        return googlePlaceDetailsResult.toString();
    }
}
