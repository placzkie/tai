var app = angular.module('app', []);

app.controller('mapController', function($scope, $rootScope, $http, $window, ReviewService, NoteService) {
    var fx = google.maps.InfoWindow.prototype.setPosition;
    google.maps.InfoWindow.prototype.setPosition = function () {
        if (this.logAsInternal) {
            google.maps.event.addListenerOnce(this, 'map_changed',function () {
                var map = this.getMap();
                if (map) {
                    google.maps.event.trigger(map, 'click', {latLng: this.getPosition()});
                }
            });
        }
        fx.apply(this, arguments);
    };
    $scope.initMap = function() {
        $window.map = new google.maps.Map(document.getElementById('map'), {
            center: {lat: 52, lng: 19},
            zoom: 6
        });
        $window.map.addListener('click', function (event) {
            addMarker(event.latLng);
            var lat = event.latLng.lat();
            var lng = event.latLng.lng();
            $rootScope.placeSelected = true;
            $http.get('/review/search?lat=' + lat + '&lng=' + lng)
                .then(function(response) {
                    var placeID = response.data.results[1].place_id;
                    $rootScope.currentPlaceID = placeID;
                    ReviewService.updateReviews(placeID)
                    NoteService.updateNotes(placeID)
                });
        });
    }
});

app.controller("reviewController", function($scope, $http, ReviewService) {
    $scope.updateReviews = ReviewService.updateReviews
});

app.controller("noteController", function($scope, $rootScope, $http, NoteService, UserPlacesService) {
    $scope.updateNotes = NoteService.updateNotes;

    $scope.addNote = function (userName) {
        var note = document.getElementById("newNoteText");
        var text = note.value;
        note.value = "";
        $http.post('/note/' + userName + '/' + $rootScope.currentPlaceID, text).then(function(response) {
            $scope.updateNotes($rootScope.currentPlaceID, userName);
            UserPlacesService.updatePlaces($rootScope.userName)
        })
    };

    $scope.removeNote = function(noteID) {
        $http.delete('/note/' + noteID).then(function(response) {
            $scope.updateNotes($rootScope.currentPlaceID, $rootScope.userName)
        })
    };

    $scope.editNote = NoteService.editNote;

    $scope.setNoteID = function(noteID, text) {
        $rootScope.editNoteID = noteID;
        $rootScope.editNoteText = text
    }
});

app.config(
    function($httpProvider) {
        $httpProvider.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
    }).controller("loginController", function($rootScope, $scope, $http, $location, UserPlacesService) {

    $http.get("/user").success(function(data) {
        if (data.name) {
            // $scope.user = data.name;
            $rootScope.userName = data.name;
            $scope.authenticated = true;
        } else {
            // $scope.user = "N/A";
            $rootScope.userName = "N/A";
            $scope.authenticated = false;
        }
        UserPlacesService.updatePlaces($rootScope.userName)
    }).error(function() {
        $rootScope.userName = "N/A";
        $scope.authenticated = false;
    });

    $scope.logout = function() {
        $http.post('logout', {}).success(function() {
            $scope.authenticated = false;
            $location.path("/");
        }).error(function(data) {
            console.log("Logout failed");
            $scope.authenticated = false;
        });
    };
});

app.controller("userPlacesController", function($rootScope, $scope, $http, ReviewService, NoteService, UserPlacesService) {
    $scope.places = $rootScope.places;
    $scope.updatePlaces = UserPlacesService.updatePlaces;

    $scope.handleClick = function(placeID, userName) {
        $rootScope.placeSelected = true;
        $rootScope.currentPlaceID = placeID;
        $scope.location = $scope.places[placeID].location;
        deleteMarkers();
        addMarker($scope.location);

        ReviewService.updateReviews(placeID);
        NoteService.updateNotes(placeID, userName);
        $('#userPlacesModal').modal('hide')
    }
});

app.service("UserPlacesService", function($rootScope, $http) {
    $rootScope.places = {};
    this.updatePlaces = function (userName) {
        $http.get('/note/' + userName + '/all')
            .then(function (response) {
                $rootScope.userPlaces = response.data
                for (var i = 0; i < $rootScope.userPlaces.length; i++) {
                    (function (placeID) {
                        $http.get('/review?placeID=' + placeID)
                            .then(function (response) {
                                $rootScope.places[placeID] = {
                                    name: response.data.name,
                                    address: response.data.formatted_address,
                                    location: response.data.geometry.location
                                }
                            });
                    })($rootScope.userPlaces[i]);
                }
            })
    };
});

app.service('NoteService', function($rootScope, $http) {
    var self = this;
    self.updateNotes = function(placeID, userName) {
        $http.get('/note/' + userName + '/' + placeID)
            .then(function (response) {
                $rootScope.notes = response.data
            });
    };

    self.editNote = function(noteID) {
        text = document.getElementById("editNoteContent").value;
        $http.put('/note/' + noteID, text).then(function(response) {
            self.updateNotes($rootScope.currentPlaceID, $rootScope.userName);
        })
    }
});

app.service('ReviewService', function($rootScope, $http) {
    this.updateReviews = function(placeID) {
        $http.get('/review?placeID=' + placeID)
            .then(function (response) {
                $rootScope.reviews = response.data.reviews;
            });
    }
});